unit CountBtn;

interface

uses
  SysUtils, Classes, Controls, StdCtrls, Dialogs;

type
  TCountBtn = class(TButton)
  private
    FCount:integer;
  protected
  public
    procedure Click;override;
    procedure ShowCount;
  published
    property Count:integer read FCount write FCount;
    constructor Create(aowner:Tcomponent);override;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('MyComponents', [TCountBtn]);
end;

constructor TCountBtn.Create(aowner:Tcomponent);
begin
  inherited create(Aowner);
end;

procedure Tcountbtn.Click;
begin
  inherited click;
  FCount:=FCount+1;
end;

procedure TCountBtn.ShowCount;
begin
  Showmessage('�� ������ '+ caption+' �� �������: '+inttostr(FCount)+' ����(�/��)');
end;

end.
