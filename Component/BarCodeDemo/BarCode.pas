unit BarCode;

interface

uses
  SysUtils, Classes, Controls, ExtCtrls, Math, Graphics;

type
  TBarCode = class(TImage)
  private
    { Private declarations }
    FdataCode:String;
    parity: array[1..9] of array[1..7] of integer;
    table : array[0..2] of array[0..9] of string;
    procedure DrawLine(x1,y1,x2,y2:integer);
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure ShowBarCode;
  published
    { Published declarations }
    property DataCode:String read FdataCode write FdataCode;
    constructor Create(aowner:Tcomponent);override;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('BarCode', [TBarCode]);
end;

constructor TBarCode.Create(aowner:Tcomponent);
begin
  inherited create(Aowner);
  parity[1][2]:= 0;
  parity[1][3]:= 0;
  parity[1][4]:= 1;
  parity[1][5]:= 0;
  parity[1][6]:= 1;
  parity[1][7]:= 1;
  parity[2][2]:= 0;
  parity[2][3]:= 0;
  parity[2][4]:= 1;
  parity[2][5]:= 1;
  parity[2][6]:= 0;
  parity[2][7]:= 1;
  parity[3][2]:= 0;
  parity[3][3]:= 0;
  parity[3][4]:= 1;
  parity[3][5]:= 1;
  parity[3][6]:= 1;
  parity[3][7]:= 0;
  parity[4][2]:= 0;
  parity[4][3]:= 1;
  parity[4][4]:= 0;
  parity[4][5]:= 0;
  parity[4][6]:= 1;
  parity[4][7]:= 1;
  parity[5][2]:= 0;
  parity[5][3]:= 1;
  parity[5][4]:= 1;
  parity[5][5]:= 0;
  parity[5][6]:= 0;
  parity[5][7]:= 1;
  parity[6][2]:= 0;
  parity[6][3]:= 1;
  parity[6][4]:= 1;
  parity[6][5]:= 1;
  parity[6][6]:= 0;
  parity[6][7]:= 0;
  parity[7][2]:= 0;
  parity[7][3]:= 1;
  parity[7][4]:= 0;
  parity[7][5]:= 1;
  parity[7][6]:= 0;
  parity[7][7]:= 1;
  parity[8][2]:= 0;
  parity[8][3]:= 1;
  parity[8][4]:= 0;
  parity[8][5]:= 1;
  parity[8][6]:= 1;
  parity[8][7]:= 0;
  parity[9][2]:= 0;
  parity[9][3]:= 1;
  parity[9][4]:= 1;
  parity[9][5]:= 0;
  parity[9][6]:= 1;
  parity[9][7]:= 0;

  table[0][0]:= '0001101';
  table[0][1]:= '0011001';
  table[0][2]:= '0010011';
  table[0][3]:= '0111101';
  table[0][4]:= '0100011';
  table[0][5]:= '0110001';
  table[0][6]:= '0101111';
  table[0][7]:= '0111011';
  table[0][8]:= '0110111';
  table[0][9]:= '0001011';
  table[1][0]:= '0100111';
  table[1][1]:= '0110011';
  table[1][2]:= '0011011';
  table[1][3]:= '0100001';
  table[1][4]:= '0011101';
  table[1][5]:= '0111001';
  table[1][6]:= '0000101';
  table[1][7]:= '0010001';
  table[1][8]:= '0001001';
  table[1][9]:= '0010111';
  table[2][0]:= '1110010';
  table[2][1]:= '1100110';
  table[2][2]:= '1101100';
  table[2][3]:= '1000010';
  table[2][4]:= '1011100';
  table[2][5]:= '1001110';
  table[2][6]:= '1010000';
  table[2][7]:= '1000100';
  table[2][8]:= '1001000';
  table[2][9]:= '1110100';
end;

procedure TBarCode.DrawLine(x1,y1,x2,y2:integer);
var i:integer;
begin
  for i:=x1 to x2 do
    begin
      Canvas.MoveTo(i,y1);
      Canvas.LineTo(i,y2);
    end;
end;

procedure TBarCode.ShowBarCode;
var temp: string;
    i,sum_even,sum_odd,crc:integer;
    fontsize,lineTop,lineBottom1,lineBottom2:integer;
    LeftText1,LeftText2,LeftText3:integer;
    lineWidth,lineStart,x1,x2:integer;
begin
  FdataCode:=Copy(FdataCode,1,12);
  FdataCode:='000000000000'+FdataCode;
  FdataCode:=Copy(FdataCode,length(FdataCode)-11,12);
  sum_even:=0;
  sum_odd:=0;
  i:=length(FdataCode);
  while i>0 do
    begin
      crc:=strtoint(FdataCode[i]);
      if odd(i) then sum_odd:=sum_odd+crc      //��������
      else sum_even:=sum_even+crc;         //������
      i:=i-1;
    end;
  sum_even:=sum_even*3;
  sum_even:=sum_even+sum_odd;
  crc:=sum_even div 10;
  crc:=crc * 10;
  crc:=crc + 10;
  crc:=crc - sum_even;

  FdataCode:=FdataCode+intToStr(crc);

  temp:= '101';
  for i:=2 to 7 do
    temp:= temp + table[parity[strtoint(FdataCode[1])][i]][strtoint(FdataCode[i])];
  temp:= temp + '01010';
  for i:= 8 to 13 do
    temp:= temp + table[2][strtoint(FdataCode[i])];
  temp:= temp + '101';
  Hint:= temp+#13#10;
  //
  //Canvas.FillRect(Canvas.ClipRect);
  //Width := 210;
  //Height:= 75;
  //Canvas.Pen.Color:= RGB(0,0,0);
  Canvas.Pen.Width:= 1;
  Canvas.Pen.Color:= clBlack;
  Canvas.Brush.Color:= clBlack;

  lineTop:= Floor((Height*4)/100);
  lineBottom1:= Floor((Height*94.67)/100);
  lineBottom2:= Floor((Height*77.33)/100);
  lineWidth:= Floor((Width*0.95)/100);
  Hint:= Hint+'lineWidth: '+intTostr(lineWidth)+#13#10;
  lineStart:= Floor((Width*7.14)/100);
  Hint:= Hint+'lineStart: '+intTostr(lineStart)+#13#10;

  i:=1;
  x1:=0;
  x2:=0;
  while (i<=190) do
    begin
      Hint:= Hint+'i: '+intTostr(i)+#13#10;
      Hint:= Hint+'(i+1)div 2: '+intToStr((i+1)div 2)+#13#10;
      Hint:= Hint+'temp[(i+1)div 2]: '+temp[(i+1)div 2]+#13#10;
      if (temp[(i+1)div 2] = '1') then
        begin
          //x1:=lineStart+((i-1)*lineWidth);
          x1:=i-1;
          Hint:= Hint+'x1 (i-1): '+intTostr(x1)+#13#10;
          x1:=x1*lineWidth;
          Hint:= Hint+'x1 (i-1)*lineWidth: '+intTostr(x1)+#13#10;
          x1:=x1+lineStart;
          Hint:= Hint+'x1: '+intTostr(x1)+#13#10;
          x2:=x1+lineWidth;
          Hint:= Hint+'x2: '+intTostr(x2)+#13#10;
          Hint:= Hint+'i+14: '+intTostr(i+14)+#13#10;
          Hint:= Hint+'i+15: '+intTostr(i+15)+#13#10;
          if (i <= 6) or ((i >= 90)and(i <= 100)) or (i >= 184) then
            begin
              DrawLine(x1,lineTop,x2,lineBottom1);
              //Canvas.Rectangle(x1,lineTop,x2,lineBottom1);
              {Canvas.MoveTo(i+14,lineTop);
              Canvas.LineTo(i+14,lineBottom1);
              Canvas.MoveTo(i+15,lineTop);
              Canvas.LineTo(i+15,lineBottom1)
              {Canvas.MoveTo(i+14,3);
              Canvas.LineTo(i+14,71);
              Canvas.MoveTo(i+15,3);
              Canvas.LineTo(i+15,71)}
            end
            else
              begin
                DrawLine(x1,lineTop,x2,lineBottom2);
                //Canvas.Rectangle(x1,lineTop,x2,lineBottom2);
                {Canvas.MoveTo(i+14,lineTop);
                Canvas.LineTo(i+14,lineBottom2);
                Canvas.MoveTo(i+15,lineTop);
                Canvas.LineTo(i+15,lineBottom2)
                {Canvas.MoveTo(i+14,3);
                Canvas.LineTo(i+14,58);
                Canvas.MoveTo(i+15,3);
                Canvas.LineTo(i+15,58)}
              end;
        end;
      i:=i+2
    end;

  Canvas.Brush.Color:= clWhite;
  Canvas.Font.Name := 'Arial';
  //Canvas.Font.Color:= rgb(0,0,0);
  //Canvas.Font.Size := 10;
  fontsize:= Floor(Height/7.5);
  Canvas.Font.Size := fontsize;
  x2:= lineStart+x2+lineWidth;
  x2:= min(x2,Width);
  LeftText1:= Floor((x2*1.9)/100);
  LeftText2:= Floor((x2*14.29)/100);
  LeftText3:= Floor((x2*57.62)/100);
  Canvas.TextOut(LeftText1, lineBottom2, FdataCode[1]);
  Canvas.TextOut(LeftText2, lineBottom2, FdataCode[2]+' '+FdataCode[3]+' '+FdataCode[4]+' '+FdataCode[5]+' '+FdataCode[6]+' '+FdataCode[7]);
  Canvas.TextOut(LeftText3, lineBottom2, FdataCode[8]+' '+FdataCode[9]+' '+FdataCode[10]+' '+FdataCode[11]+' '+FdataCode[12]+' '+FdataCode[13]);
  {Canvas.TextOut(4, 58, FdataCode[1]);
  Canvas.TextOut(30, 58, FdataCode[2]+' '+FdataCode[3]+' '+FdataCode[4]+' '+FdataCode[5]+' '+FdataCode[6]+' '+FdataCode[7]);
  Canvas.TextOut(121, 58, FdataCode[8]+' '+FdataCode[9]+' '+FdataCode[10]+' '+FdataCode[11]+' '+FdataCode[12]+' '+FdataCode[13]);}

end;

end.
